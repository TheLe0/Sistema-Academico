public class Turma
{
    private String codigo;
    private String horario;
    private int alunos;
    private String sala;
    private Disciplina disciplina;
    private Aluno aluno;
    private Curso curso;
        
    public void setCodigo(String codigoT){
        codigo = codigoT;
    }
    
    public String getCodigo(){
        return codigo;
    }
    
    public void setHorario(String horarioT){
           horario = horarioT;
    }
    
    public String getHorario(){
        return horario;
    }
    
    public void setAlunos(int qtde){
        alunos = qtde;
    }
    
    public int getAlunos(){
        return alunos;
    }
    
    public void setSala(String salaT){
        sala = salaT;
    }
    
    public String getSala(){
        return sala;
    }
    
    public void setDisciplina(Disciplina disciplinaT){
        disciplina = disciplinaT;
    }
    
    public Disciplina getDisciplina(){
        return disciplina;
    }
    
    public void setAluno(Aluno al){
        aluno = al;
    }
    
    public Aluno getAluno(){
        return aluno;
    }
    
    public void setCurso(Curso course){
        curso = course;
    }
    
    public Curso getCurso(){
        return curso;
    }
}
