public class Curso
{
    private String name;
    private String codigo;
    private String turno;
    private int alunos;
    
    public Curso(){
        alunos = 0;
    }
    
    public void setName(String nomeAl){
        name = nomeAl;
    }
    
    public String getName(){
        return name;
    }
    
    public void setCodigo(String code){
        codigo = code;   
    }
    
    public String getCodigo(){
        return codigo;
    }
    
    public void setTurno(String turn){
        turno = turn;
    }
    
    public String getTurno(){
        return turno;
    }
    
    public void setAlunos(int als){
        alunos = als;
    }
    
    public int getAlunos(){
        return alunos;
    }
}
