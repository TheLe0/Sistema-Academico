public class ListaDeAluno
{
    private Aluno[] osAlunosDaLista = new Aluno[100];
    private int tamanho = 0;
    private int capacidade = 100;
    
    public ListaDeAluno(int capacidade) {
        this.capacidade = capacidade;
        osAlunosDaLista = new Aluno[capacidade];
    }
    
    public int incluir(Aluno aluno) {
        osAlunosDaLista[tamanho] = aluno;
        tamanho++;
        
        if (tamanho >= capacidade ) {
            return 0;
        }
        return 1;
    }
    
    public int incluir(Aluno aluno, int indice) {
     
        if ((indice < capacidade) && (indice < tamanho) && (indice > 0)) {
                
                Aluno aux = osAlunosDaLista[indice];
                osAlunosDaLista[indice] = aluno;
            
            for(int i = (indice + 1); i < tamanho; i++) {
                 osAlunosDaLista[i] = aux;
                
                 aux = osAlunosDaLista[i];
            } 
            tamanho++;
            return 1;       
        }
        return 0;
    
    }
    
    public void incluirOrdenado(Aluno aluno) {
            
            if (incluir(aluno) == 1) {
                ordena();
            }
    }
    
    public Aluno procurar(String CPF){
        Aluno alu = new Aluno();
        
        for(int i = 0; i <= tamanho; i++){
            if(osAlunosDaLista[i].getCpf().equals(CPF)){
                alu = osAlunosDaLista[i];
                return alu;
            }
        }                
        return null;
    }
    
    public int tamanho() {
        return tamanho;
    }
    
    public int remover(int id) {
        
        if(id >= 0 && id <= tamanho && id <= capacidade){
           
            for(int i = id; i < tamanho ; i++){
                osAlunosDaLista[i] = osAlunosDaLista[i+1];
            }
            tamanho--;
            return 1;

        }
           
        return 0;
    }
    
    public Aluno getAluno(int pos) {
        Aluno al = new Aluno();    
        al = osAlunosDaLista[pos];
        
        return al;
    }
    
    public void ordena() {
        
        for (int i = 1; i < tamanho; i++){
            
            Aluno aux = osAlunosDaLista[i];
            int j = i;
            
            while ((j > 0) && (osAlunosDaLista[j-1].getNome().compareTo(aux.getNome()) < 0 )){
                osAlunosDaLista[j] = osAlunosDaLista[j-1];
                j -= 1;
            }
            osAlunosDaLista[j] = aux;
                
        }       
    }    
}
