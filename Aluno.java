public class Aluno
{
    private String nome = null;
    private String cpf = null;
    private int creditos;
    private Curso curso = null;
    
    public Aluno(){
        
        
    }
    
    public String getNome(){
        return nome;
    }
    
    public void setNome(String nomeAl){
        nome = nomeAl;
    }
    
    public String getCpf(){
        return cpf;
    }
    
    public void setCpf(String CPF){
        cpf = CPF;
    }
 
    public void setCurso(Curso cul){
        curso = cul;
    }
    
    public Curso getCurso(){
        return curso;
    }
    
    public void setCreditos(int creds){
        creditos = creds;
    }
    
    public int getCreditos(){
        return creditos;
    }
}
