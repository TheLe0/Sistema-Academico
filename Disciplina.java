public class Disciplina
{
    private String nome;
    private String codigo;
    private String descricao;
    private Professor prof;
    private int vagas;
    private ListaDeAluno alunosMatriculados;
    private int numeroDeAlunosMatriculados;
    
    public Disciplina(String codigo, String nome, int vagas) {        
        this.codigo = codigo;
        this.nome = nome;
        alunosMatriculados = new ListaDeAluno(vagas);
        numeroDeAlunosMatriculados = 0;                
    }
        
    public void setNome(String nomeD){
        nome = nomeD;   
    }
    
    public String getNome(){
        return nome;
    }
    
    public void setCodigo(String codigoD){
        codigo = codigoD;   
    }
    
    public String getCodigo(){
        return codigo;
    }
    
    public void setDescricao(String descricaoD){
        descricao = descricaoD;
    }
    
    public String getDescricao(){
        return descricao;
    }
    
    public void setVagas( int vags){
        vagas = vags;
    }
    
    public int getVagas(){
        return vagas;
    }
    
    public void setMatriculas(int num){
        numeroDeAlunosMatriculados = num;
    }
    
    public int getMatriculas(){
        return numeroDeAlunosMatriculados;
    }
    
    public void setProfessor(Professor profe){
        prof = profe;
    }
    
    public Professor getProfessor(){
        return prof;
    }
    
    public boolean matricular(Aluno aluno){        
        if(numeroDeAlunosMatriculados <= vagas) {
            alunosMatriculados.incluir(aluno);
            numeroDeAlunosMatriculados++;
            return true;
        } 
        return false;
    }
    
    public int haveVagas(){
        return vagas - numeroDeAlunosMatriculados;
    }    
}
