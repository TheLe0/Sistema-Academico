public class Professor
{

    private String nome;
    private String cpf;
    private Turma turma;
    
    public void setNome(String nomeP){
        nome = nomeP;   
    }
    
    public String getNome(){
        return nome;
    }
    
    public void setCpf(String cpfP){
        cpf = cpfP;
    }
    
    public String getCpf(){
        return cpf;
    }
    
    public void setTurma(Turma turmaP){
        turma = turmaP;
    }
    
    public Turma getTurma(){
        return turma;
    }
}
