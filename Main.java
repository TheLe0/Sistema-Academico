import java.util.Scanner;
import java.util.Locale;
public class Main
{
    private void menu(){
           System.out.println("----------------------");
           System.out.println("| 1- Cadastrar Aluno: ");
           System.out.println("| 2- Imprimir Cadastros: ");
           System.out.println("| 3- Cadastrar Curso: ");
           System.out.println("| 4- Imprimir Cursos: ");
           System.out.println("| 5- Cadastrar Disciplina: ");
           System.out.println("| 6- Imprimir Disciplina ");
           System.out.println("| 7- Cadastrar Professor: ");
           System.out.println("| 8- Imptimir Professor: ");
           System.out.println("| 9- Cadastrar Turma: ");
           System.out.println("| 0- Imprimir Turma: ");
           System.out.println("| 10- Cadastrar informacoes adicionais: ");
           System.out.println("| Outra tecla - Sair"); 
           System.out.println("-------------------------");
    }
  
    public static void main(String[] args) {
       Scanner input;
       input = new Scanner(System.in);
       input.useLocale(Locale.ENGLISH);
       int opc = 1;
       Aluno[] cadastro = new Aluno[10];
       Curso[] curso = new Curso[10];
       Disciplina[]  disciplina = new Disciplina[10];
       Professor[] professor = new Professor[10];
       Turma[] turma = new Turma[10];
       int count = 0;
       int cont = 0;
       int cc = 0;
       int ct = 0;
       int cnt = 0;
       double valCred = 0;
       
       while (opc != 0 ) {
           Main principal = new Main();
           principal.menu();
           opc = input.nextInt();
                       
           switch(opc){
               
              case 1:
                   System.out.println("-------------- CADASTRAR ALUNO ------------");
                   if(cont <= 0){  
                     System.out.println("Deves antes cadastrar um curso!");
                     break;
                   }
                                      
                   Aluno al = new Aluno();
                   System.out.println("Nome: ");
                   input.nextLine();
                   al.setNome(input.nextLine());
                   System.out.println("CPF: ");
                   al.setCpf(input.nextLine());
                   System.out.println("Numero de creditos cursando: ");
                   al.setCreditos(input.nextInt());
                   System.out.println("Cdigo do Curso: ");
                   input.nextLine();
                   String CodCurso = input.nextLine();
                   
                   for(int i = 0; i<cont;i++){
                       
                       if(curso[i].getCodigo().equals(CodCurso)){
                         al.setCurso(curso[i]);
                         curso[i].setAlunos(curso[i].getAlunos() +1);
                         break;
                       }
                   }
                    
                    if(al.getCurso().equals(null)) {
                        al.setCurso(null);
                        System.out.println("Curso não encontrado!");
                        
                    }
               
                    if(count > 0){
                    
                        for(int i =0; i<count; i++){
                            if(al.getCpf().equals(cadastro[i].getCpf())){
                                System.out.println("CPF j� est� cadastrado para outro aluno!");
                                break;
                            }
                        }                      
                    }
                    
                    
                   cadastro[count] = al;
                   
                   
                   count++;
                   System.out.println("---------------------------------");
                   break;
               case 2:
                    System.out.println("-------------- ALUNO ----------------- ");
                    for(int i = 0; i<count;i++) {
                        System.out.println(cadastro[i].getNome());
                        System.out.println(cadastro[i].getCpf());
                        System.out.println(cadastro[i].getCurso());
                        System.out.println(cadastro[i].getCreditos());
                        
                        if (valCred != 0) { 
                            System.out.println("Valor total:  "+(cadastro[i].getCreditos()*valCred));
                            System.out.println("Mensalidade:  "+((cadastro[i].getCreditos()*valCred)/6));
                        }
                    }
                    System.out.println("---------------------------------------- ");
                    break;
               case 3:
                       System.out.println("------------ CADASTRAR CURSO ------------------ ");
                       Curso cs = new Curso();
                       System.out.println("Nome do curso: ");
                       input.nextLine();
                       cs.setName(input.nextLine());
                       System.out.println("Código: ");
                       cs.setCodigo(input.nextLine());
                       System.out.println("Turno: ");
                       cs.setTurno(input.nextLine());
                       
                       if(cont > 0){
                           for(int i =0; i<cont;i++){
                               if(cs.getCodigo().equals(curso[i].getCodigo())){
                                   System.out.println("Codigo j� est� cadastrado em um outro curso!");
                                   break;
                                }
                           }
                       }
                       curso[cont] = cs;
                
                
                       cont++;
                       System.out.println("--------------------------");
                       break;
               case 4:
                     System.out.println("---------------- CURSO ------------");
                     for(int i = 0; i<cont;i++) {
                         System.out.println(curso[i].getName());
                         System.out.println(curso[i].getCodigo());
                         System.out.println(curso[i].getTurno());
                        }
                     System.out.println("--------------------------------------");
                     break;
               case 5:
                       System.out.println("--------------- CADASTRAR DISCIPLINA ------------");
                       System.out.println("Nome da disciplina: ");
                       input.nextLine();
                       String nomeDisc = input.nextLine();
                       System.out.println("Codigo: ");
                       String CodDisc = input.nextLine();
                       System.out.println("Quantidade de vagas: ");
                       int VagDisc = input.nextInt();
                       Disciplina aux = null;
               
               
                       Disciplina disp = new Disciplina(CodDisc,nomeDisc, VagDisc);
                       System.out.println("Descricao: ");
                       input.nextLine();
                       disp.setDescricao(input.nextLine());
                      
                       
                    if(disciplina.length >= 0) {
                            System.out.println("Aluno: ");
                            String nomeAl = input.nextLine();
                            for(int i = 0; i<VagDisc; i++){
                               
                               if(CodDisc.equals(disciplina[i].getCodigo())){
                                   aux = disciplina[i];
                                   break;
                                }                                                
                            }
                        
                        
                        if(aux != null) {
                            for(int i = 0; i<VagDisc;i++){
                             
                                if(nomeAl.equals(cadastro[i])){
                                    boolean resultado = aux.matricular(cadastro[i]);
                                    
                                    if(!resultado) {
                                        System.out.println("Vagas esgotadas!");
                                    }
                                }                                 
                            }
                        } else {
                            System.out.println("Aluno nao existe!");
                        }
                    } else {
                        System.out.println("Deve haver pelo mnenos um aluno cadastrado!");
                    }
                       
                        if(cc > 0){
                            for(int i = 0; i < cc; i++){
                                if(disciplina[i].getCodigo().equals(disp.getCodigo())){
                                    System.out.println("C�digo j� cadastrado em outra disciplina!");
                                    break;
                                }
                            }
                        }
                    
                       disciplina[cc] = disp;                       
                       cc++;
                       System.out.println("----------------------------------------");
                       break;
               case 6:
                      System.out.println("----------------------- DISCIPLINA -------------------");
                      for(int i=0; i<cc;i++){
                          System.out.println(disciplina[i].getNome());
                          System.out.println(disciplina[i].getCodigo());
                          System.out.println(disciplina[i].getDescricao());
                      }
                      System.out.println("---------------------------------------------------------");
                       break;
               case 7:
                       System.out.println("------------------ CADASTRAR PROFESSOR ---------------------");
                       if(cont <= 0){  
                           System.out.println("Deves antes cadastrar um curso!");
                           break;
                       }
               
                       Professor prof = new Professor();
                       System.out.println("Nome do Professor: ");
                       input.nextLine();
                       prof.setNome(input.nextLine());
                       System.out.println("CPF do professor: ");
                       prof.setCpf(input.nextLine());
                       System.out.println("C�digo Turma: ");
                       String CodTurma = input.nextLine();
                       
                        for(int i = 0; i<cnt;i++){
                       
                            if(turma[i].getCodigo().equals(CodTurma)){
                                prof.setTurma(turma[i]);
                                break;
                            }
                        }
                    
                        if(prof.getTurma() == null) {
                            prof.setTurma(null);
                            System.out.println("Turma nao encontrada!");
                        
                        }
                        
                        if(ct > 0) {
                            for(int i = 0; i < ct; i++){
                                if(professor[i].getCpf().equals(prof.getCpf())){
                                    System.out.println("CPF informado ja esta cadastrado para outro professor!");
                                    break;
                                }
                            }
                        }
               
                        professor[ct] = prof;
                   
                   
                        ct++;
                       
                       System.out.println("--------------------------------------------");
                       break;
                       
               case 8:
                      System.out.println("------------------- PROFESSOR ---------------------");
                      for(int i=0; i<ct;i++){
                          System.out.println(professor[i].getNome());
                          System.out.println(professor[i].getCpf());
                          System.out.println(professor[i].getTurma());
                      }              
                      System.out.println("-----------------------------------------------------"); 
                       break;
               case 9:
                       System.out.println("------------------- CADASTAR TURMA ------------------");
                       if(cc <= 0){  
                           System.out.println("Deves antes cadastrar uma disciplina!");
                           break;
                       }
               
               
                       Turma tur = new Turma();
                       System.out.println("Digite o c�digo da turma: ");
                       input.nextLine();
                       tur.setCodigo(input.nextLine());
                       System.out.println("Digite o hor�rio da turma: ");
                       tur.setHorario(input.nextLine());
                       System.out.println("Digite o n�mero de alunos matriculados: ");
                       tur.setAlunos(input.nextInt());
                       System.out.println("Digite a sala da disciplina: ");
                       input.nextLine();
                       tur.setSala(input.nextLine());
                       System.out.println("Digite o codigo da disciplina: ");
                       String CodDisci = input.nextLine();
                       
                       for(int i = 0; i<cc; i++){
                             if(disciplina[i].getCodigo().equals(CodDisci)){
                                tur.setDisciplina(disciplina[i]);
                                break;
                            }                           
                        }
                        
                        if(tur.getDisciplina().equals(null)) {
                            tur.setDisciplina(null);
                            System.out.println("Disciplina não encontrada!");                        
                        }
                        
                        System.out.println("Digite o codigo do curso: ");
                        String Cod_Curso = input.nextLine();
                        
                        for(int i =0; i<cont;i++){
                            if(curso[i].getCodigo().equals(Cod_Curso)){
                                tur.setCurso(curso[i]);
                            }                            
                        }
                        
                        if(tur.getCodigo().equals(null)){
                            tur.setCurso(null);
                            System.out.println("Curso nao encontrado!");
                            
                        }
                        
                        if(cnt > 0 ){
                            for(int i = 0; i < cnt ;i++){
                                if(turma[i].getCodigo().equals(tur.getCodigo())){
                                    System.out.println("Codigo informado ja foi cadastrado para uma outra turma!");
                                    break;
                                }
                            }
                        }
                                                
                        turma[cnt] = tur;                   
                        System.out.println("-------------------------------------------------------");
                        cnt++;
                       
                
                      break;
               case 0:
                      System.out.println("------------------ TURMA -------------------------");
                       for(int i=0; i<cnt;i++){
                          System.out.println(turma[i].getCodigo());
                          System.out.println(turma[i].getHorario());
                          System.out.println(turma[i].getHorario());
                          System.out.println(turma[i].getAlunos());
                          System.out.println(turma[i].getSala());
                          System.out.println(turma[i].getDisciplina());
                          
                      }              
                      System.out.println("-------------------------------------------------------");
               
                      opc = 1;
                      break;
              case 10:
                       System.out.println("------------------INFOS ADICIONAIS ------------------------ ");
                       System.out.println("Valor do credito ");
                       valCred = input.nextDouble();
                       
                       if(valCred <= 0 ){
                           System.out.println("O valor do cr�dito nao pode ser nulo!");
                           valCred = 0;
                        }
              
                       break;
               default:
                    System.out.println("------------- FIM ---------------------------------");
                    opc = 0;
           }  
       }
       input.close();
    }
}
