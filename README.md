# Sistema-Academico
Exercício da disciplina - Programação Orientada a Objetos - Marcos Eduardo Casa. 2018/2

O Sistema deverá incluir classes que definem uma interface do sistema com o usuário. É importante que as classes de interface com usuário realizem toda a interação (outras classes, do núcleo do sistema, não devem envolver elementos de interface com usuário)
