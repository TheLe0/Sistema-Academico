public class Nodo
{
    private Aluno info ;
    private Nodo proximo ;

    public void setInfo(Aluno aluno)
    {
        this.info = aluno ;
    }

    public Aluno getInfo()
    {
        return this.info ;
    }

    public void setProximo(Nodo nodo)
    {
        this.proximo = nodo;
    }

    public Nodo getProximo()
    {
        return this.proximo ;
    }
}





